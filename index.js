const scrapeIt = require('scrape-it')
const models = require('./models')
const availableUrl = 'http://parkavewestpdx.securecafe.com/availableunits.aspx'
const DECIMAL = 10
const emptyPromise = new Promise((resolve, reject) => {
  resolve([])
})

function parseRent (rentString, index) {
  // $2,288-$2,922
  var parts = rentString.split('-')
  var part = parts[index]
  part = part.replace('$', '').replace(',', '')
  part = parseInt(part, DECIMAL)
  return part
}

function saveToDatabase (results) {
  console.log('Saving')
  if (process.env.DRYRUN) {
    console.log('Dry run, seeding units')
    return models.Unit.seed().then(() => {
      return models.Rent.sync()
    })
  }
  return Promise.all(
    results.rents.map((rent) => {
      return models.Rent.create(rent).catch(() => {
        // Seed when these is an exception incase the record or table needs to be created
        console.log('Missing', rent.UnitNumber)
        return models.Audit.sync().then(() => {
          return models.Unit.seed().then(() => {
            return models.Rent.sync()
          })
        })
      })
    })
  )
}

module.exports = function handler (message) {
  if (message.time) {
    return scrapeRent(message)
  }
}

function scrapeRent (message) {
  console.log('Scraping')
  return scrapeIt(availableUrl, {
    rents: {
      listItem: '.AvailUnitRow',
      data: {
        UnitNumber: {
          selector: '[data-label="Apartment"]',
          convert: x => parseInt(x.replace('#', ''), DECIMAL)
        },
        min: {
          selector: '[data-label="Rent"]',
          convert: (x) => parseRent(x, 0)
        },
        max: {
          selector: '[data-label="Rent"]',
          convert: (x) => parseRent(x, 1)
        }
      }
    }
  }).then(saveToDatabase).then((results) => {
    if (process.env.DRYRUN) {
      return emptyPromise
    }
    return models.Audit.create({
      date: message.time,
      result: `${results.length} records`
    }).then(() => null)
  }).catch((err) => {
    console.log('Caught err', err)
    if (process.env.DRYRUN) return emptyPromise
    return models.Audit.create({
      date: message.time,
      result: err.toString()
    }).then(() => null)
  })
}
