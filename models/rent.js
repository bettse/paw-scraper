const Sequelize = require('sequelize')

module.exports = function (sequelize, DataTypes) {
  const Rent = sequelize.define('Rent', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    date: { type: Sequelize.DATEONLY, defaultValue: Sequelize.NOW },
    min: Sequelize.INTEGER,
    max: Sequelize.INTEGER
  })
  return Rent
}
