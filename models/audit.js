const Sequelize = require('sequelize')

module.exports = function (sequelize, DataTypes) {
  const Audit = sequelize.define('Audit', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    date: { type: Sequelize.DATEONLY, defaultValue: Sequelize.NOW },
    result: Sequelize.STRING
  })

  return Audit
}
